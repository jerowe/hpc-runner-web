requires 'perl', '5.008005';
requires "Moose";

# requires 'Some::Module', 'VERSION';

requires "Archive::Tar";
requires "Cwd";
requires "Data::Dumper";
requires "DateTime";
requires "File::Basename";
requires "File::Path";
requires "File::Slurp";
requires "File::Spec";
requires "Git::Wrapper";
requires "Git::Wrapper::Plus::Branches";
requires "Git::Wrapper::Plus::Ref::Tag";
requires "Git::Wrapper::Plus::Tags";
requires "IO::Handle";
requires "IO::Interactive";
requires "IPC::Cmd";
requires "IPC::Open3";
requires "Moose";
requires "MooseX::App";
requires "MooseX::App::Command";
requires "MooseX::App::Role::Log4perl";
requires "Perl::Version";
requires "Sort::Versions";
requires "String::CamelCase";
requires "Symbol";
requires "Test::More";
requires "namespace::autoclean";
requires "String::Escape";
requires "YAML";
requires 'YAML::XS';
requires 'DateTime::HiRes';
requires 'HPC::Runner';
requires 'HPC::Runner::MCE';

on test => sub {
    requires 'Test::More', '0.96';
};
