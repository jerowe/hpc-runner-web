use strict;
use warnings;

package HPC::Runner::Web;

use MooseX::App qw(Color Config);

our $VERSION = '0.08';

use IPC::Cmd;
use Cwd qw(getcwd);

with 'MooseX::Object::Pluggable';

has project_dir => (
    is	    => 'rw',
    isa 	=> 'Str',
    default => sub {return getcwd()},
);

sub execute_command{
    my($self, $cmd, $info) = @_;
    my $buffer = "";

    if( scalar IPC::Cmd::run( command => $cmd,
            verbose => 0,
            buffer  => \$buffer )
    ) {
        $self->log->info("$info: $buffer\n") if $info;
    }
    else{
        $self->log->warn("Something went wrong with the cmd: $cmd!\n");
    }
}

1;

__END__

=encoding utf-8

=head1 NAME

HPC::Runner::Web - Blah blah blah

=head1 SYNOPSIS

  use HPC::Runner::Web;

=head1 DESCRIPTION

HPC::Runner::Web is

=head1 AUTHOR

Jillian Rowe E<lt>jillian.e.rowe@gmail.comE<gt>

=head1 COPYRIGHT

Copyright 2015- Jillian Rowe

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

=cut
