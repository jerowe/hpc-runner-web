use strict;
use warnings;
package HPC::Runner::Web::Git;

use Moose::Role;

use namespace::autoclean;
use Git::Wrapper;
use Git::Wrapper::Plus::Ref::Tag;
use Git::Wrapper::Plus::Tags;
use Git::Wrapper::Plus::Branches;
use Cwd;
use File::Path qw(make_path);
use File::Slurp;
use File::Spec;
use Perl::Version;
use Sort::Versions;
use Archive::Tar;

has 'git_dir' => (
    is => 'rw',
    isa => 'Str',
    default => sub {return getcwd()},
);

has 'git' => (
    is => 'rw',
    #lazy => 1,
);

has 'current_branch' => (
    is => 'rw',
    isa => 'Str',
    #lazy => 1,
    predicate => 'has_current_branch',
);

has 'version' => (
    is => 'rw',
    #isa => 'Str|Perl::Version',
    #default => '0.0.1',
    predicate => 'has_version',
);

has 'remote' => (
    is => 'rw',
    isa => 'Str',
    #lazy => 1,
    predicate => 'has_remote',
);

sub init_git{
    my($self) = shift;

    my $git = Git::Wrapper->new($self->git_dir);

    ##Make sure we are in the top dir
    my @output = $git->rev_parse(qw(--show-toplevel));
    $self->git_dir($output[0]);
    $git = Git::Wrapper->new($self->git_dir);

    $self->git($git);
}

sub dirty_run{
    my($self) = shift;

    #my $statuses = $self->git->status;
    my $dirty_flag = $self->git->status->is_dirty;

    if($dirty_flag){
        #die print "There are uncommited files in your repo!\nPlease commit these files before running.";
        print "There are uncommited files in your repo!\nPlease commit these files before running.";
    }
}

sub git_info{
    my $self = shift;

    $self->branch_things;
    $self->get_version;
}

sub branch_things{
    my($self) = @_;

    my $branches = Git::Wrapper::Plus::Branches->new(
        git => $self->git
    );

    my $current;
    for my $branch ( $branches->current_branch ) {
        $self->current_branch($branch->name);
    }
}

sub git_config{
    my($self) = @_;

    #First time we run this we want the name, username, and email
    my @output = $self->git->config(qw(--list));

    my %config = ();
    foreach my $c (@output){
        my @words = split /=/, $c;
        $config{$words[0]} = $words[1];
    }
    return \%config;
}

sub git_logs{
    my($self) = shift;

    my @logs = $self->git->log;
    return \@logs;
}

sub get_version{
    my($self) = shift;

    #$DB::single=2;
    return if $self->has_version;

    my $tags_finder = Git::Wrapper::Plus::Tags->new(
        git => $self->git
    );

    #print "Tags are\n";
    my @versions = ();
    for my $tag ( $tags_finder->tags ) {
        my $name = $tag->name;
        if($name =~ m/(\d+)\.(\d+)\.(\d+)/){
            push(@versions, $name);
        }
    }

    if(@versions){
        my  @l = sort {versioncmp($a, $b)} @versions;
        my $v = pop(@l);
        my $pv = Perl::Version->new($v);
        $pv->inc_subversion;
        $self->version($pv);
    }
    else{
        $self->version("0.0.1");
    }
    $self->git_push_tags;
}

sub git_push_tags{
    my($self) = shift;

    my @remote = $self->git->remote;

    $self->git->tag($self->version);

    foreach my $remote (@remote){
        $self->git->push({tags => 1}, $remote);
    }
}

sub create_release{
    my($self) = @_;

    my @filelist = $self->git->ls_files();

    #make git_dir/archive if it doesn't exist
    Archive::Tar->create_archive( $self->git_dir."/archive/".$self->project."-".$self->version.".tgz", COMPRESS_GZIP, @filelist);
}


#__PACKAGE__->meta->make_immutable;
1;
