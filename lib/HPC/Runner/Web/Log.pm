package HPC::Runner::Web::Log;

#use Moose::Role;
use Moose;

use File::Path qw(make_path remove_tree);
use Data::Dumper;
use namespace::autoclean;
use YAML;
use YAML::XS 'LoadFile';
use DateTime;
use Data::Dumper;
use DateTime::HiRes qw();
use List::Uniq ':all';
use File::Details;

with 'HPC::Runner::Web::Git';
extends 'HPC::Runner';

has 'tags' => (
    is => 'rw',
    isa => 'ArrayRef',
    required => 0,
    default => sub {
        return [];
    },
);

has '+logdir' => (
    default => \&set_weblogdir,
);

has 'dt' => (
    is => 'rw',
    isa => 'DateTime',
    default => sub { return DateTime->now(); }
);

## Dependency things
has afterok => (
    is        => 'rw',
    isa     => 'ArrayRef',
    required => 0,
    default => sub {
        return [];
    },
);

has 'env_tags' => (
    traits  => ['Array'],
    is => 'rw',
    isa => 'ArrayRef',
    default => sub {
        return ['SAMPLE'];
    },
    handles => {
        has_env_tags    => 'count',
        has_no_env_tags => 'is_empty',
    },
);

sub git_things{
    my $self = shift;

    $self->init_git;
    $self->dirty_run;
    $self->git_info;
    push(@{$self->tags}, "$self->{version}");
    my @tmp = uniq(@{$self->tags});
    $self->tags(\@tmp);
}

sub set_weblogdir {
    my $self = shift;
    my $logdir;

    $DB::single=2;

    $self->git_things;

    my $yaml = LoadFile($self->git_dir."/.project.yml") or die print "Could not open .project.yml! Is this a HPC::Runner::Web project? $!\n";
    $DB::single=2;

    $logdir = $yaml->{'BlogDir'};
    $logdir .= "/".$self->version;

    $logdir = $logdir."/".$self->logname."_".$self->set_logfile;
    $logdir =~ s/\.md//g;
    $logdir =~ s/\.log//g;

    $DB::single=2;

    make_path($logdir) if ! -d $logdir;

    return $logdir;
};

sub env_log {
    my $self = shift;

    my $tt = $self->dt;
    $tt =~ s/T/ /g;
    $tt =~ s/[^\w]/_/g;

    open(my $p, ">>".$self->logdir."/ENV_".$tt.".md") or die print "Couldn't open log file what is happening here!? $!\n";

    $DB::single=2;

    $tt = $self->dt;
    $tt =~ s/T/ /g;

    $tt = DateTime::HiRes->now(time_zone => 'local')->strftime('%F %T.%5N');
    my $hash = {title => "ENV", tags => $self->tags, date => "$tt"};
    print $p Dump $hash;
    print $p "---\n\n";

    print $p "## Environment\n\n";
    #Print modules if they are there
    my $buffer = "";
    $DB::single=2;
    print $p "### Modules\n" if $ENV{'LOADEDMODULES'};
    print $p "```bash\n".$ENV{'LOADEDMODULES'}."\n```\n" if $ENV{'LOADEDMODULES'};

    print $p "### Path\n";
    my $path = $ENV{'PATH'};
    if($path){
        my @tmp = split(':', $path);
        print $p "```bash\n".join("\n", @tmp)."\n```\n";
    }
    $DB::single=2;

    print $p "## Git Things\n\n";

    $DB::single=2;
    print $p "Current branch: ".$self->current_branch."\n";
    print $p "Current version: ".$self->version."\n";

    close $p;

}



sub set_logfile {
    my $self = shift;

    my $dt = DateTime->now();
    $self->dt($dt);
    $dt =~ s/[^\w]/_/g;
    return "$dt.md";
};

before 'init_log' => sub {
    my $self = shift;

    my $tt = $self->logfile;
    $tt =~ s/\.log/\.md/g;

    $self->logfile($tt);

};

after 'init_log' => sub {
    my $self = shift;

    open(my $p, ">>".$self->logdir."/".$self->logfile) or die print "Couldn't open log file what is happening here!? $!\n";

    my $tt = $self->dt;
    my($lf, $cc, $pid) = $self->get_title;

    $DB::single=2;

    ##TODO change this to support other blog times
    #$tt = DateTime::HiRes->now(time_zone => 'local')->strftime('%F %T.%5N');

    $self->print_log_yaml($p, $lf, $tt, $pid);

    close $p;
};

sub print_log_yaml {
    my $self = shift;
    my $p = shift;
    my $lf = shift;
    my $tt = shift;
    my $pid = shift;

    $DB::single=2;
    my $hash = {title => $lf, tags => $self->tags, date => "$tt"};

    if($self->has_env_tags){
        foreach my $env (@{$self->env_tags}){
            next unless $env;
            push(@{$self->tags}, $ENV{$env});
        }
    }

    if($self->job_scheduler_id){
        push(@{$self->tags}, "SchedulerID_$self->{job_scheduler_id}");
    }
    @{$self->tags} = uniq(@{$self->tags});

    print $p Dump $hash;
    print $p "---\n\n";

    print $p "### DateTime: $tt\n";
    if($pid){
        print $p "### ProcessID: $pid\n";
    }
    if($self->job_scheduler_id){
        print $p "### SchedulerID: ".$self->job_scheduler_id."\n";
    }
}

sub get_title{
    my $self = shift;
    my($tt, $lf, $pid, $cc);

    $lf = $self->logfile;
    $lf =~ s/\.md//;

    if($lf =~ m/^(CMD\d+)_/){
        my $t = $1;
        if($lf =~ m/^CMD\d+_(\d+)_PID/){
            $cc = $1;
        }
        if($lf =~ m/PID_(\d+)_/){
            $pid = $1;
        }
        $lf = $t;
    }
    elsif($lf =~ m/^MAIN/){
        $lf = "MAIN";
    }

    if($self->logname){
        if($cc){
            $lf = $self->logname." $lf $cc";
        }
        else{
            $lf = $self->logname." $lf";
        }
    }

    return($lf, $cc, $pid);
}

#Avoid stupidly long log files - or hexo will DIE
#
before 'log_messages' => sub {
    my $self = shift;
    my $level = shift;
    my $message = shift;
    my $cmdpid = shift;

    my $details = File::Details->new($self->logdir."/".$self->logfile);
    my $size = $details->size();

    #10000
    if($size > 10000){
        my $lf = $self->logfile;
        if($lf =~ m/^CMD(\d+)_PID/){
            my $n = $1;
            $lf =~ s/^CMD$n\_PID/CMD$n\_1_PID/;
            $DB::single=2;
            $self->logfile($lf);
            $self->command_log($self->init_log);
        }
        elsif($lf =~ m/^CMD(\d+)_(\d+)_PID/){
            my(@match) = $lf =~  m/^CMD(\d+)_(\d+)_PID/;
            my($n, $nn) = ($match[0], $match[1]);
            my $old = $nn;
            $nn = $nn + 1;
            $lf =~ s/^CMD$n\_$old\_PID/CMD$n\_$nn\_PID/;
            $self->logfile($lf);
            $self->command_log($self->init_log);
        }
    }
};

#__PACKAGE__->meta->make_immutable;
1;
