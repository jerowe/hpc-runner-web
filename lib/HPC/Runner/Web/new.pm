use strict;
use warnings;

package HPC::Runner::Web::new;

use IPC::Cmd qw[can_run];
use File::Basename;
use Cwd;
use String::CamelCase qw(camelize decamelize wordsplit);
use File::Path qw(make_path remove_tree);
use String::Escape qw( quote  );
use YAML::XS;

use MooseX::App::Command;
extends 'HPC::Runner::Web';
with 'MooseX::App::Role::Log4perl';

parameter 'projectname' => (
    is                => 'rw',
    isa               => 'Str',
    documentation     => q[Your Project Name],
    required          => 1,
);

#TODO
#Add support for other notebook types (hugo, octopress)
#Right now we are only supporting hexo

command_short_description 'Create a new project';
command_long_description 'This creates a new project, initializes git, and creates the hexo lab notebook';

sub execute{
    my $self = shift;

    can_run('npm') or $self->log->warn("NPM is not installed or not in your path! You must install Nodejs/npm in order to view your new lab notebook!");
    can_run('hexo') or $self->log->warn("Hexo is not installed or not in your path! You must install hexo in order to view your new lab notebook!");

    my $project = $self->projectname;
    $self->projectname(camelize($self->projectname));

    $DB::single=2;
    make_path($self->projectname."/lib");
    make_path($self->projectname."/conf");
    make_path($self->projectname."/script");
    make_path($self->projectname."/test");
    make_path($self->projectname."/graph");
    make_path($self->projectname."/www");
    make_path($self->projectname."/archive");
    make_path($self->projectname."/data");
    make_path($self->projectname."/scratch");

    chdir $self->projectname;
    $self->gen_project_yml;

    $self->gen_gitignore;

    $self->log->info("By default ./data and ./archive are added to your git ignore.");
    chdir 'www' or die $self->log->fatal("Could not change to www directory!");

    $self->log->info("Creating new notebook this could take some time...");
    $self->execute_command("hexo init ".quote($self->projectname)) or die $self->log->fatal("Could not run hexo init");

    $DB::single=2;

    chdir $self->projectname or die $self->log->fatal("Could not cd to projectdir");

    $self->execute_command("npm install", "Installing npm packages... This could take some take") or die $self->log->fatal("Could not complete npm install!");

    #$self->execute_command("npm install hexo-generator-feed --save");

    $self->log->info("Setup complete!");
}

sub gen_project_yml {
    my($self)  = @_;

    open(my $p, ">.project.yml") or die print "Couldn't open a file for writing! $!\n";

    my $hash = {ProjectName => $self->projectname, TopDir => getcwd(), BlogDir => getcwd()."/www/".$self->projectname."/source/_posts"};
    print $p Dump $hash;

    close $p;

    $self->log->info("$self->{projectname} Initialized...");
}

sub gen_gitignore {
    my $self = shift;

    $self->execute_command("git init", "Git repo successfully initialized : ");

    open(my $p, ">.gitignore") or die print "Couldn't open a file for writing! $!\n";

    print $p "data\n";
    print $p "archive\n";
    print $p "www\n";
    print $p "scratch\n";

    $self->execute_command("git add .gitignore", "Added .gitignore...");

    close $p;
}


1;
