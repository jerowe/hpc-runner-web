use strict;
use warnings;
package HPC::Runner::Web::run;

use Cwd;
use Moose;
use MooseX::App::Command;
use Data::Dumper;

extends qw(HPC::Runner::Web);

command_strict '0';

command_short_description 'Run your notebook';
command_long_description 'Run your notebook';

=head1 Attributes

=cut

=head2 work_cmds

Options to pass to your workflow runner (MCE,PBS,etc)

=cut

option 'work_cmds' => (
    is => 'rw',
    isa => 'HashRef',
    documentation => 'commands to pass to your job runner',
    required => 1,
);

option 'tags' => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    documentation => 'tags to add to your blog',
    cmd_split          => qr/,/,
);

=head2 plugins

    --plugins MCE,PBS,MYCOOLPLUGIN

=cut

option 'plugins' => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    documentation => 'Add Plugins to your run - default is MCE',
    cmd_split => qr/,/,
);


has 'work_dir' => (
    is => 'rw',
    isa => 'Str',
    default => sub { return getcwd() },
);

sub execute{
    my $self = shift;

    $DB::single=2;
    $self->load_plugins(@{$self->plugins}) if $self->plugins;

    $DB::single=2;
    $self->run_notebook($self->work_cmds, $self->tags);
}

1;

=encoding utf-8

=head1 NAME

HPC::Runner::Web::run - Run your workflows through a lab notebook

=head1 SYNOPSIS

  use HPC::Runner::Web;

=head1 DESCRIPTION

HPC::Runner::Web::run is a way to implement bash like workflows with logging.

In other words, LOG ALL THE THINGS!

=head1 AUTHOR

Jillian Rowe E<lt>jillian.e.rowe@gmail.comE<gt>

=head1 COPYRIGHT

Copyright 2015- Jillian Rowe

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

=cut
